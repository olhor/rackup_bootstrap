require 'bundler'
Bundler.require

env = Sprockets::Environment.new

# add paths from rails-assets
RailsAssets.load_paths.each do |path|
  env.append_path path
end

# add views paths
env.append_path 'views'

# add assets paths
env.append_path 'assets/js'
env.append_path 'assets/css'
env.append_path 'assets/images'

run env