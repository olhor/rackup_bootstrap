# Welcome

## Features

This is a rack-based web application bootstrap using:

 * embedded ruby
 * sprockets for asset management
 * sassy css (scss)
 * coffee-script
 * RACK

## Usage

Can be used for single / multi page web apps.

## Demo

[rack.keyworks.pl](http://rack.keyworks.pl)